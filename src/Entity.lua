--[[
    GD50
    Super Mario Bros. Remake

    -- Entity Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu
]]

function shallowcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function deepcopy(orig, currentdepth, maxdepth)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' and currentdepth < maxdepth then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key, currentdepth+1, maxdepth)] = deepcopy(orig_value, currentdepth + 1, maxdepth)
        end
        setmetatable(copy, deepcopy(getmetatable(orig), currentdepth+1, maxdepth))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

Entity = Class{}

function Entity:init(def)
    -- position
    self.x = def.x
    self.y = def.y

    -- velocity
    self.dx = 0
    self.dy = 0

    -- dimensions
    self.width = def.width
    self.height = def.height

    self.texture = def.texture
    self.stateMachine = def.stateMachine
    self.currentStateName = ""

    self.direction = 'left'

    -- reference to tile map so we can check collisions
    self.map = def.map

    -- reference to level for tests against other entities + objects
    self.level = def.level

    self.pastStates = {}
    self.pastStatesNum = 0

    self.toCapture = def["rewindProperties"]
    self.canRewind = false
    if self.toCapture ~= nil and #self.toCapture > 0
    then
        self.canRewind = true
        currentState = {}
        for k in pairs(self.toCapture)
        do
            currentState[k] = deepcopy(self[k], 0, 2)
        end
        currentState['_count'] = 0.0
        table.insert(self.pastStates, currentState)
        self.pastStatesNum = 1
    end

    self.dying = false
    self.dead = false
end

function Entity:changeState(state, params)
    self:changeStateInternal(state, params, true)
end

function Entity:changeStateInternal(state, params, dorewind)
    self.stateMachine:change(state, params)
    self.currentStateName = state

    if dorewind and self.canRewind then
        local newState = {}
        for k, prop in pairs(self.toCapture)
        do
            newState[prop] = deepcopy(self[prop], 0, 2)
        end
        newState['_count'] = 0.0
        self.pastStates[self.pastStatesNum] = newState 
        
        --print("Replacing state")
        --print("-- Anim timer: " .. tostring(self.currentAnimation.timer))
        --print('-- Frame: ' .. tostring(self.currentAnimation.currentFrame))
        --print("STATE Anim timer: " .. tostring(newState.currentAnimation.timer))
        --print('STATE Frame: ' .. tostring(newState.currentAnimation.currentFrame))
        
        --table.insert(self.pastStates, currentState)
        --self.pastStatesNum = self.pastStatesNum + 1
    end
end

function Entity:update(dt)
    if self.canRewind
    then
        self.pastStates[self.pastStatesNum]['_count'] = self.pastStates[self.pastStatesNum]['_count'] + dt
        --for k, prop in pairs(self.toCapture)
        --do
            --print("property: " .. tostring(k))
            --print("property: " + tostring(k) + " has value: " + tostring(self[k]))
            --currentState[prop] = self[prop]
        --end
    end

    --print("Current state")
    --print(dump(currentState))

    self.stateMachine:update(dt)

    if self.canRewind
    then
        local currentState = self.pastStates[self.pastStatesNum]
        for k, prop in pairs(self.toCapture)
        do
            if currentState[prop] ~= self[prop] then
                --print('Property differs')

                local newState = {}
                for k, prop in pairs(self.toCapture)
                do
                    --print("property: " .. tostring(k))
                    --print("property: " + tostring(k) + " has value: " + tostring(self[k]))
                    newState[prop] = deepcopy(self[prop], 0, 2)
                end
                newState['_count'] = 0.0
                --print("Adding state")
                --print("-- Anim timer: " .. tostring(self.currentAnimation.timer))
                --print('-- Frame: ' .. tostring(self.currentAnimation.currentFrame))
                --print("STATE Anim timer: " .. tostring(newState.currentAnimation.timer))
                --print('STATE Frame: ' .. tostring(newState.currentAnimation.currentFrame))
                table.insert(self.pastStates, newState)
                self.pastStatesNum = self.pastStatesNum + 1
                return
            end
        end
        -- Nothing has changed from prev state
        --print("Nothing changed from prev state")
        --self.pastStates[self.pastStatesNum]['_count'] = self.pastStates[self.pastStatesNum]['_count'] + 1
    end
end

function Entity:rewind(time)
    if not self.canRewind then
        return
    end

    local rewtime = time
    local mstate = self.pastStates[self.pastStatesNum]
    while self.pastStatesNum > 2 and (mstate["_count"] <= rewtime)
    do
        rewtime = rewtime - mstate["_count"]
        --print('moving back one state')
        for k, prop in pairs(self.toCapture)
        do
            self[prop] = mstate[prop]
        end
        -- replay the state
        --print("PUTBACK Anim timer: " .. tostring(self.currentAnimation.timer))
        --print('PUTBACK Frame: ' .. tostring(self.currentAnimation.currentFrame))

        table.remove(self.pastStates, self.pastStatesNum)
        self.pastStatesNum = self.pastStatesNum - 1
        mstate = self.pastStates[self.pastStatesNum]
    end
    mstate["_count"] = mstate["_count"] - rewtime
end

function Entity:clearHistory()
    self.pastStates = {}
    self.pastStatesNum = 0
end

function Entity:collides(entity)
    return not (self.x > entity.x + entity.width or entity.x > self.x + self.width or
                self.y > entity.y + entity.height or entity.y > self.y + self.height)
end

function Entity:render()
    love.graphics.draw(gTextures[self.texture], gFrames[self.texture][self.currentAnimation:getCurrentFrame()],
        math.floor(self.x) + 8, math.floor(self.y) + 10, 0, self.direction == 'right' and 1 or -1, 1, 8, 10)
end