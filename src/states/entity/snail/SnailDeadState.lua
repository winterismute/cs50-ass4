SnailDyingState = Class{__includes = BaseState}

function SnailDyingState:init(snail)
    self.snail = snail
    self.animation = Animation {
        frames = {51},
        interval = 1
    }
    self.snail.currentAnimation = self.animation
end

function SnailDyingState:enter(params)
    self.snail.dx = 0
    self.snail.dy = 50
end

function SnailDyingState:update(dt)
    if self.snail.y <= WINDOW_HEIGHT then
        self.snail.y = self.snail.y + (self.snail.dy * dt)
    end
end