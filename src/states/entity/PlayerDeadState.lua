
PlayerDeadState = Class{__includes = BaseState}

function PlayerDeadState:init(player)
    self.player = player
    self.animation = Animation {
        frames = {1},
        interval = 1
    }
    self.player.currentAnimation = self.animation
end

function PlayerDeadState:enter(params)
    self.player.dx = 0
    self.player.dy = 110
end

function PlayerDeadState:update(dt)
    if self.player.y <= WINDOW_HEIGHT then
        self.player.y = self.player.y + (self.player.dy * dt)
    end
end